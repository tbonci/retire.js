FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:11-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-2.2.2}

ARG NPM_VERSION
ENV NPM_VERSION ${NPM_VERSION:-6.14.8}

ARG YARN_VERSION
ENV YARN_VERSION ${YARN_VERSION:-1.22.5}

# Version of Python, defaults to Python 3
ARG DS_PYTHON_VERSION
ENV DS_PYTHON_VERSION ${DS_PYTHON_VERSION:-3}

RUN set -ex; \
    # ensure we have the most recent versions of all installed packages
    apk upgrade --update-cache --available; \
    \
    # install git for project with git-sourced dependencies \
    apk add --no-cache git build-base python python3; \
    \
    # install for python and python 3 \
    pip3 install --upgrade pip setuptools; \
    pip install --upgrade pip setuptools; \
    \
    # temporary workaround for \
    # https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011 \
    npm config set unsafe-perm true; \
    \
    # remove version of yarn pre-installed in node:11-alpine base image \
    rm -rf /usr/local/bin/yarn /usr/local/bin/yarnpkg /opt/yarn-v1.15.2/; \
    \
    # install retire.js and yarn \
    npm install -g retire@$SCANNER_VERSION yarn@$YARN_VERSION; \
    \
    # upgrade npm itself \
    npm install -g npm@$NPM_VERSION;

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
