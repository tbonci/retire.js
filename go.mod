module gitlab.com/gitlab-org/security-products/analyzers/retire.js/v2

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.20.4
)

go 1.13
